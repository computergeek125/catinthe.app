---
layout: page
title: Projects
paginate:
  collection: projects
---

<ul>
  {% for project in collections.projects.resources %}
    <li>
      <a href="{{ project.relative_url }}">{{ project.data.title }}</a>
    </li>
  {% endfor %}
</ul>
