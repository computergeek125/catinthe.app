---
layout: project
title:  "XIV Map Copilot"
---

XIV Map Copilot is a small helper website to help FFXIV players running treasure hunting parties to
keep track of flags.

You **WILL** need a resource pack to run it.  I am not currently providing one publicly since I am
still working on some of the finer details.

Current production version is on [Github Pages](https://computergeek125.github.io/xiv-map-copilot/copilot-app)  
Source code is on [Github](https://github.com/computergeek125/xiv-map-copilot)  