---
# Feel free to add content and custom Front Matter to this file.

layout: default
title: Homepage
---

# The Cat in the App

This is a small gathering point of my personal projects as I throw them out to the world.  This site
is brand new, so if something is empty or using a default template here, please don't mind the dust,
its under construction.

I make no warranties or guarantees to project quality or fitness to a specific purpose, or even if
they will be developed forever.  I only promise that I will try my best to make these as bug-free
and up-to-date as one person with an unrelated full-time job can handle.

List of projects is up in the top navigation.

Basically all (if not entirely all) of these projects should be open-source on one of my SCM
profiles:
- [Gitlab \| computergeek125](https://gitlab.com/computergeek125)
- [Github \| computergeek125](https://github.com/computergeek125)

